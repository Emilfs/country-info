import rdflib
import csv
import pandas as pd #for handling csv and csv contents
from rdflib import Graph, Literal, RDF, RDFS, URIRef, Namespace, OWL #basic RDF handling
from rdflib.namespace import FOAF , XSD #most common namespaces
import urllib.parse #for parsing strings to URI's

country_dataset=pd.read_csv('countries of the world.csv')
country_dataset.insert(1, "Country Name", country_dataset['Country'])
country_dataset.insert(3, "Region Name", country_dataset['Region'])
country_dataset['Country'] = country_dataset['Country'].str.strip()
country_dataset['Country'] = country_dataset['Country'].str.replace(" ","_")
country_dataset['Region'] = country_dataset['Region'].str.strip()
country_dataset['Region'] = country_dataset['Region'].str.replace(" ","")
country_dataset['Country Name'] = country_dataset['Country Name'].str.strip()
country_dataset['Region Name'] = country_dataset['Region Name'].str.strip()
country_dataset = country_dataset.replace(",",".", regex=True)
country_dataset

graph_country = Graph()
country = Namespace('http://example.org/country/')
continent = Namespace('http://example.org/continent')
population = Namespace('http://example.org/population')
area = Namespace('http://example.org/area')
birth_rate = Namespace('http://example.org/birth')
literacy = Namespace('http://example.org/literacy')
gdp = Namespace('http://example.org/gdp')
schema = Namespace('http://schema.org')
dbo = Namespace('http://dbpedia.org/ontology/')
dbr = Namespace('http://dbpedia.org/resource/')

for index, row in country_dataset.iterrows():
    graph_country.add((URIRef(country+row['Country']),RDF.type, URIRef(dbo+'country')))
    graph_country.add((URIRef(country+row['Country']),OWL.sameAs, URIRef(dbr+row['Country'])))
    graph_country.add((URIRef(country+row['Country']),RDFS.label, Literal(row['Country Name'], datatype=XSD.string)))
    graph_country.add((URIRef(country+row['Country']),URIRef(continent), Literal(row['Region Name'], datatype=XSD.string)))
    graph_country.add((URIRef(country+row['Country']),URIRef(area), Literal(row['Area (sq. mi.)'], datatype=XSD.integer)))
    graph_country.add((URIRef(country+row['Country']),URIRef(population), Literal(row['Population'], datatype=XSD.integer)))
    graph_country.add((URIRef(country+row['Country']),URIRef(gdp), Literal(row['GDP ($ per capita)'], datatype=XSD.decimal)))
    graph_country.add((URIRef(country+row['Country']),URIRef(literacy), Literal(row['Literacy (%)'], datatype=XSD.decimal)))
    graph_country.add((URIRef(country+row['Country']),URIRef(birth_rate), Literal(row['Birthrate'], datatype=XSD.decimal)))

graph_country.serialize('mycsv2rdf.ttl',format='turtle')