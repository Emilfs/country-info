import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import App from '../App';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

const showInfoBox = () => {
    App.handleExpandClick();
};

export default function SimpleList() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <List component="nav" aria-label="main mailbox folders">

        <ListItem button onClick={showInfoBox}>
          <ListItemText primary="Nama Fasilitas" secondary="terletak di nasa center ini"/>
        </ListItem>
        
        <Divider />

        <ListItem button onClick={showInfoBox}>
          <ListItemText primary="Nama Fasilitas" secondary="terletak di nasa center ini"/>
        </ListItem>
        
        <Divider />

        <ListItem button onClick={showInfoBox}>
          <ListItemText primary="Nama Fasilitas" secondary="terletak di nasa center ini"/>
        </ListItem>

      </List>

    </div>
  );
}
