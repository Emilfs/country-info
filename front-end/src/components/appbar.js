import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import logo from '../assets/logo.svg';
import { withStyles } from '@material-ui/core/styles';

function ElevationScroll(props) {
    const { children } = props;
    const trigger = useScrollTrigger();

    return React.cloneElement(children, {
        elevation: trigger ? 4 : 0,
    });
}

const useStyles = theme => ({
    gambar: {
        width: 'auto',
        height: '50px',
        marginRight: '10px',
    },
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 375,
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 4,
    },
    appbar: {
        backgroundColor: '#40a0d9',
    },
});

class SearchAppBar extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            category: 'state',
            query: '',
        };
    }

    setsearchby = (text) => {
        this.setState({category: text});
    }

    ubah = (event) => {
        this.setState({query: event.target.value});
    }

    panggil = (event) => {
        event.preventDefault();
        this.props.getInfo({
            query: this.state.query, 
        });
    }

    render() {
        const {classes} = this.props;
        
        return (
            <React.Fragment>

                <ElevationScroll {...this.props}>
                    <AppBar className={classes.appbar}>
                        <Toolbar>
                            <img item src={logo} alt="logo" className={classes.gambar}></img>

                            <Paper item component="form" className={classes.root}>
                                <InputBase
                                    className={classes.input}
                                    placeholder="Search Country"
                                    inputProps={{ 'aria-label': 'search google maps' }}
                                    onChange={this.ubah}
                                />

                                <IconButton type="submit" className={classes.iconButton} aria-label="search">
                                    <SearchIcon onClick={this.panggil} />
                                </IconButton>
                            </Paper>

                        </Toolbar>
                    </AppBar>
                </ElevationScroll>

                <Toolbar />

            </React.Fragment>
        )
    }
};

export default withStyles(useStyles) (SearchAppBar);