import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles(theme => ({
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    margin: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  action: {
    width: '100%',
  },
}));

export default function RecipeReviewCard(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.card}>
      <CardHeader
        disableTypography
        title={props.datakirim.countryLabel.value}
        // subheader={props.datakirim.state_name}

      />

      <Divider />

      <CardContent>
      {
          (props.datakirim.capitalLabel !== undefined) ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Capital : </strong>{props.datakirim.capitalLabel.value}
            </Typography>
          </div>
        : null
        }
        {
          (props.datakirim.comment !== undefined) ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Description : </strong>{props.datakirim.comment.value}
            </Typography>
          </div>
        : null
        }
      </CardContent>

      <Divider />

      <CardActions disableSpacing className={classes.action}>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>

      </CardActions>

      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>More info:</Typography>

        {
          (props.datakirim.continent !== undefined) ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Continent : </strong>{props.datakirim.continent.value} 
            </Typography>
          </div>
        : null
        }
        <br/>
        {
          (props.datakirim.currencyLabel !== undefined) ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Currency : </strong>{props.datakirim.currencyLabel.value}
            </Typography>
          </div>
        : null
        }
        <br/>
        {
          (props.datakirim.anthemTitle!== undefined) ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>National Anthem : </strong>{props.datakirim.anthemTitle.value}
            </Typography>
          </div>
        : null
        }
        <br/>
        {
          (props.datakirim.population.value !== "") ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Population : </strong>{props.datakirim.population.value}
            </Typography>
          </div>
        : null
        }
        <br/>
        {
          (props.datakirim.area.value !== "") ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Area : </strong>{props.datakirim.area.value} km2
            </Typography>
          </div>
        : null
        }
        <br/>
        {
          (props.datakirim.birth.value !== "") ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Birth Rate : </strong>{props.datakirim.birth.value}%
            </Typography>
          </div>
        : null
        }
        <br/>
        {
          (props.datakirim.gdp.value !== "") ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Gross Domestic Product : </strong>{props.datakirim.gdp.value}
            </Typography>
          </div>
        : null
        }
        <br/>
        {
          (props.datakirim.literacy.value !== "") ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Literacy Rate : </strong>{props.datakirim.literacy.value}%
            </Typography>
          </div>
        : null
        }
        <br/>
        {
          (props.datakirim.demonym.value !== "") ? 
          <div>
            <Typography variant="body2" component="p" align="left">
            <strong>Demonym : </strong>{props.datakirim.demonym.value}
            </Typography>
          </div>
        : null
        }
        <br/>
        </CardContent>
      </Collapse>

    </Card>
  );
}
