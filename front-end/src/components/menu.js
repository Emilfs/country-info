import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles } from '@material-ui/core/styles';

var data = [
    'Country name', 'Continent'
];

const useStyles = () => ({
    tombol: {
        textTransform: 'none',
        // fontWeight: '600',
    },
});

class CustomMenu extends React.Component {
    constructor(props){
        super(props)
        this.state = { 
            anchorEl: null, 
            text: 'Country name', 
        };
    };

    handleClick = event => {
        this.setState({
            anchorEl: event.currentTarget,
        })
    };

    handleClose = (text2) => {
        this.setState({
            anchorEl: null,
            text: text2,
        });
        // this.props.fungsi(this.state.text);
        this.props.fungsi(text2);
        // alert(text2);
    };

    handleClose2 = () => {
        this.setState({
            anchorEl: null,
        });
        this.props.fungsi(this.state.text);
    };


    render()
    {
        const {classes} = this.props;
        return (
            <div>
                <Button 
                    aria-controls="simple-menu"
                    aria-haspopup="true" 
                    onClick={this.handleClick} 
                    // inputProps={{
                    //     textTransform: 'none'
                    //   }}
                    className={classes.tombol}
                    >
                        {this.state.text}
                    <ExpandMoreIcon />
                </Button>

                <Menu
                    id="simple-menu"
                    anchorEl={this.state.anchorEl}
                    keepMounted
                    open={Boolean(this.state.anchorEl)}
                    onClose={this.handleClose2}
                >
                    {
                        data.map(
                            (i) => (
                            <MenuItem onClick={
                                // this.handleClose(i)
                                () => {
                                    // alert(i);
                                    this.handleClose(i)
                                  
                                }
                        
                            }>{i}</MenuItem>
                            )
                        )            
                    }
                </Menu>
            </div>
        );
    }
}

export default withStyles(useStyles) (CustomMenu);