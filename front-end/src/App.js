import React from 'react';
import './App.css';
import { Grid, CssBaseline, Paper, Container } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import RecipeReviewCard from './components/card';
import ElevateAppBar from './components/appbar';

const useStyles = theme => ({
    app: {
        flexGrow: 1,
        maxWidth: '752',
    },
    root2: {
        padding: theme.spacing(3, 2),
        maxWidth: '500px',
        marginLeft: theme.spacing(10),
    },
    grid: {
        marginTop: theme.spacing(5),
        width: '100%',
        padding: theme.spacing(3),
    },
    list: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
});

class App extends React.Component {
    constructor(props){
        super(props)
        this.state = {openclose: false, index2: 0, data:[],info:"", detail: ""};
        this.getInfo = this.getInfo.bind(this);
    }
    
    getDetail(query) {
        if(query){
            const url = 'http://localhost:5000/country/'+ query;
            const response = fetch(url);
            response.then( res => {
                if(res.status === 200){
                    return res.json()
                }
            }).then( resJson => {
                this.setState({detail:resJson})
                console.log(this.state.detail)
            })  
            
        }
    }   

    getInfo(query) {
        if(query){
            let info = query.query;
            const url = 'http://localhost:5000/listCountry/'+ info;
            const response = fetch(url);
            response.then( res => {
                if(res.status === 200){
                    return res.json()
                }
            }).then( resJson => {
                this.setState({info:resJson})
            })  
        }
    }   

    async componentDidMount() {
        this.getInfo();
        console.log(this.state.info);
    }

    setopenclose = () => {
        this.setState({openclose: true});
    }
    
    setindex2 = (index) => {
        this.setState({index2: index})
    }

    render() {
        const { classes } = this.props;
        return (
            <div className="App" >
                <CssBaseline />
                <ElevateAppBar getInfo={ this.getInfo } />
            
                <Container maxWidth="lg" >
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="flex-center"
                        spacing={10}
                        className={classes.grid}
                    >
                        
                        <Grid item xs={12} md={6} >
                            <Paper className={classes.root2} >
                                <Container maxWidth="md">
                                    <List component="nav" aria-label="main mailbox folders" className={classes.list}>
                                        {
                                            (this.state.info && this.state.info.length !== 0) ?
                                                this.state.info.results.bindings.map((p, index) => (
                                                    <div>
                                                        <ListItem button onClick={() => {
                                                            this.setopenclose();
                                                            this.setindex2(parseInt(index));  
                                                            this.getDetail(this.state.info.results.bindings[index].countryLabel.value)                                   
                                                        }}>
                                                            <ListItemText primary={p.countryLabel.value} secondary={p.continent.value} />
                                                        </ListItem>
                                                        <Divider />
                                                    </div>
                                                ))
                                                : <h1>No Search Result</h1>
                                        }
                                    </List>
                                </Container>
                            </Paper>
                            </Grid>

                        <Grid item xs={12} md={6} >
                        <Paper className={classes.root2} >
                            { (this.state.openclose && this.state.detail) ? <RecipeReviewCard datakirim={this.state.detail.results.bindings[0]} /> : <h1>No Result</h1>}
                            </Paper>
                        </Grid>
                        
                    </Grid>
                </Container>

            </div>
        )
    }
};

export default withStyles(useStyles)(App);
