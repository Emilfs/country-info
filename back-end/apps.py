#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask, request
import json
from rdflib import URIRef, BNode, Literal, Namespace, Graph, XSD
from rdflib.namespace import RDF
import requests
from flask_cors import CORS
from SPARQLWrapper import SPARQLWrapper, JSON

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():

    if request.method == 'POST':
        content = request.json
        a = content['searchby']
        b = content['search_text']
        c = {'status': 200}
        print(a)
        print(b)
        return json.dumps(c)

    return 'Server Works!'


@app.route('/country/<country>')
def countryLocal(country="Malaysia"):
    inputUser = country.replace(" ","_")
    sparql = SPARQLWrapper("http://localhost:3030/country-info/sparql")
    sparql.setQuery("""
        PREFIX country: <http://example.org/country/>
        PREFIX local: <http://example.org/>
        PREFIX dbr: <http://dbpedia.org/resource/>
        PREFIX dbp: <http://dbpedia.org/property/>
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>

        Select *
        Where {
            country:""" + inputUser + """ rdfs:label ?countryLabel ;
                            local:continent ?continent ;
                            local:population ?population ;
                            local:area ?area ;
                            local:birth ?birth ;
                            local:literacy ?literacy ;
                            local:gdp ?gdp ;
                            owl:sameAs ?remote .
            }
    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    sparql1 = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql1.setQuery("""
        PREFIX country: <http://example.org/country/>
        PREFIX local: <http://example.org/>
        PREFIX dbr: <http://dbpedia.org/resource/>
        PREFIX dbp: <http://dbpedia.org/property/>
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>

        Select ?capitalLabel ?anthemTitle ?currencyLabel ?demonym ?comment
        Where {
            { """ + " <"+ results["results"]["bindings"][0]["remote"]["value"] + "> " + """ dbo:capital ?capital ;
                    dbo:anthem|dbp:nationalAnthem ?anthem ;
                    dbo:currency ?currency ;
                    dbo:demonym ?demonym;
                    rdfs:comment ?comment .
                ?capital rdfs:label ?capitalLabel .
                ?currency rdfs:label ?currencyLabel . } OPTIONAL
                 { ?anthem dbp:title ?anthemTitle . }
                FILTER langMatches(lang(?capitalLabel),'en') .
                FILTER langMatches(lang(?comment),'en') .
                FILTER langMatches(lang(?demonym),'en') .
                FILTER langMatches(lang(?currencyLabel),'en') .  
            }
            Limit 1
    """)
    sparql1.setReturnFormat(JSON)
    results1 = sparql1.query().convert()

    results['head']['vars'] = results['head']['vars'] + results1['head']['vars']
    results["results"]["bindings"][0].update(results1["results"]["bindings"][0])

    return json.dumps(results)


@app.route('/listCountry/<country>')
def listCountry(country):
    inputUser = country.lower().title()
    sparql = SPARQLWrapper("http://localhost:3030/country-info/sparql")
    sparql.setQuery("""
        PREFIX country: <http://example.org/country/>
        PREFIX local: <http://example.org/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

        Select *
        Where {
            ?country rdfs:label ?countryLabel ;
            local:continent ?continent ;
                    FILTER regex(?countryLabel, """ + "'" + inputUser + "'" + """)
            }
        LIMIT 10
        """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    return json.dumps(results)
