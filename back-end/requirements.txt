certifi==2020.12.5
chardet==3.0.4
click==7.1.2
Flask==1.1.2
Flask-Cors==3.0.9
idna==2.10
isodate==0.6.0
itsdangerous==1.1.0
Jinja2==2.11.2
MarkupSafe==1.1.1
pyparsing==2.4.7
rdflib==5.0.0
requests==2.25.0
six==1.15.0
urllib3==1.26.2
Werkzeug==1.0.1
